# Texte du site

## Page : _A propos_

Je suis un jeune photographe amateur de 18 ans. J’habite dans la Loire et je suis étudiant en informatique avec l’objectif de devenir développeur.

J’ai donc décidé de créer ce site web qui mélange deux de mes passions : le développement et la photographie. Ainsi, vous pouvez observer sur ce site en même temps mes photos et mes capacités en création de site web.

Je ne me considère pas comme un grand photographe mais une personne qui aime prendre des photos en essayant de nouvelles compositions, styles de retouche, … Je vous partage les photographies que j’arrive à prendre, ainsi vous pouvez voir mon évolution au cours du temps. Le fait de vous partager mes clichés me permet de me motiver à prendre de nouvelles photos tout en essayant de les améliorer.
Mes photos sont prises avec un Nikon D5300 ou avec un Samsung Galaxy A51, les éventuelles retouches sont aussi faites par moi même.

A côté de la photographie je suis aussi passionné par la technologie, j’aime visionner des films et des séries et jouer aux jeux vidéos.

## Page : _Home_

### Section : _A propos_

Je suis un jeune photographe amateur de 18 ans.

Passionné par la technologie et l'informatique, je suis aussi intéressé par la photographie. Je profite de mon temps libre et de mes vacances pour prendre en photo les paysages, la faune et la flore.

### Section : _Ou retrouver mes photos ?_

Vous pouvez voir un aperçu de mes photos dans l’onglet Galerie de ce site.
Pour avoir accès à tous mes clichés simplement et rapidement depuis votre smartphone, vous pouvez me suivre sur mon compte Instagram ou sur mon compte 500px qui est une plateforme de partage de photographies.
