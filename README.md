# Site photographie

## Présentation

Voici un projet personnel d'un site web permettant de partager une de mes passions : la photographie.

![Page *Home* du site](/Documentation/screen_site.png)

## Contenu

Se projet est composé de plusieurs parties :

- HTML : Pour le contenu du site
- CSS : Pour la partie design
- JavaScript : Pour certaines fonctionnalités du site (Lightbox / dark mode / event scroll)
- Base de données : Pour stocker les images présentes dans la galerie
- PHP : Pour lier le site web et la base de données
- Images : Photos et logo contenu dans le site

## Information

Toutes les photos contenu dans le site ont été prises par moi même.
<br>
La majorité des logos présent sur le site ont été créé par moi même (à l'exception de _camera.svg_, _dark_mode.svg_ et _background_mini_galerie.webp_)

## Lien du site

[caydo-photography.go.yo.fr](https://caydo-photography.go.yo.fr/)
