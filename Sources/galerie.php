<!DOCTYPE html>
<html lang="fr" data-theme="light">
  <head>
    <meta charset="utf-8" />
    <meta
      name="description"
      content="Portfolio photographe amateur - GALERIE"
    />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>CAYDO - Galerie</title>
    <link rel="shortcut icon" href="images/ressources/camera.ico" />
    <link rel="stylesheet" href="css/importation_et_variable.css" />
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/header.css" />
    <link rel="stylesheet" href="css/footer.css" />
    <link rel="stylesheet" href="css/galerie.css" />
    <link
      rel="stylesheet"
      href="css/mobile.css"
      media="screen and (max-width: 790px)"
    />
    <link
      rel="stylesheet"
      href="css/mobile_autres.css"
      media="screen and (max-width: 790px)"
    />

    <script src="js/dark_mode.js"></script>
    <script src="js/lightbox.js" type="module"></script>
    <script src="js/event_scroll.js" type="module"></script>
    <noscript>Votre navigateur ne supporte pas le JavaScript!</noscript>
  </head>

  <body>
    <header>
      <a href="home.html" id="titrePage">CAYDO</a>

      <nav id="nav">
        <ul>
          <li><a href="home.html">HOME</a></li>
          <li id="IndicateurPageActuel">
            <a href="galerie.php">GALERIE</a>
          </li>
          <li><a href="a_propos.html">A PROPOS</a></li>
          <li><a href="contact.html">CONTACT</a></li>
        </ul>
      </nav>

      <button id="dark_mode" onclick="Button_DarkMode(0, true)"></button>
    </header>

    <main>
      <h2>GALERIE</h2>
      <div id="galerie_flexbox">
        <?php
          // Définition des variables de connection
          define('HOST','localhost');
          define('DB_NAME','rqmmrysn_database');
          define('USER','rqmmrysn_viewer');
          define('PASS','UP#5tKt2o7KU#9;?2A');
          
          try {
            // Connection à la base de données
            $db = new PDO("mysql:host=" . HOST . ";dbname=" . DB_NAME, USER, PASS);
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          
            // Requête SQL
            $sqlQuery = 'SELECT * FROM photos ORDER BY date DESC';
            $sth = $db->prepare($sqlQuery);
            $sth->execute();
            $photos = $sth->fetchAll(PDO::FETCH_ASSOC);
          
            // Tableau pour convertir le format du mois
            $months = [
            "01" => "janvier",
            "02" => "février",
            "03" => "mars",
            "04" => "avril",
            "05" => "mai",
            "06" => "juin",
            "07" => "juillet",
            "08" => "août",
            "09" => "septembre",
            "10" => "octobre",
            "11" => "novembre",
            "12" => "décembre",
            ];
          
            // On affiche les photos une par une
            foreach($photos as $value) {
              $date = explode("-",$value[date]);
              $month = $months[$date[1]];
              $titre = utf8_encode($value[titre]);
              $lieu = utf8_encode($value[lieu]);
              // Insertion du code HTML (main)
              echo <<<HTML
              <div class="card">
                <img
                  src="$value[chemin]"
                  alt="$titre - $lieu;$date[2] $month $date[0]"
                  loading="lazy"
                />
                <div class="detail">
                  <a href="$value[chemin]">VOIR PLUS</a>
                  <a
                    href="$value[lien_insta]"
                    target="_blank"
                    ><img src="images/ressources/instagram_white.svg" loading="lazy"
                  /></a>
                </div>
              </div>
              HTML;
          }
          } 
          // Message d'erreur en cas d'échec
          catch(PDOException $e) {
            echo <<< HTML
            <p>Erreur avec la base de données</p>
            HTML;
          }
        ?>
      </div>
    </main>

    <footer>
      <div id="premiere_ligne">
        <div id="liens_footer">
          <p>Ressources</p>
          <ul>
            <li><a href="home.html">Home</a></li>
            <li><a href="galerie.php">Galerie</a></li>
            <li><a href="a_propos.html">A propos</a></li>
            <li><a href="contact.html">Contact</a></li>
          </ul>
        </div>

        <div id="texte_footer">
          <p id="titre_footer">CAYDO</p>
          <p>Photographe amateur</p>
          <hr />

          <a
            class="lien_contact_footer"
            href="mailto:caydo.photography@gmail.com"
            target="_blank"
          >
            <img src="images/ressources/mail_white.svg" alt="logo mail"/>
            <p>caydo.photography@gmail.com</p>
          </a>
        </div>

        <div id="arrow">
          <a href="#">
            <img src="images/ressources/button_up.svg" alt="logo retour vers le haut"/>
          </a>
        </div>
      </div>

      <hr />

      <p id="copyright">Copyright © 2022 - Justin Carvalheiro (Caydo)</p>
    </footer>
  </body>
</html>
