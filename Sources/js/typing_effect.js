const textes = ["Photographie", "Animaux", "Paysages", "Fleurs"];
typewriter(textes, 0, 0);

/**
 * Affiche un texte lettre par lettre, puis appelle deleteWord pour supprimer le mot
 * @param {string[]} textes Listes des textes à écrire
 * @param {number} num Indice du mot actuel
 * @param {number} index Indice de la lettre actuel
 */
function typewriter(textes, num, index) {
    const title = document.querySelector("h1");
    if (index < textes[num].length) {
        setTimeout(() => {
            title.innerHTML += `<span>${textes[num][index]}</span>`;
            typewriter(textes, num, index + 1);
        }, 200);
    } else {
        setTimeout(() => {
            deleteWord(textes, num, index);
        }, 2000);
    }
}

/**
 * Supprime un texte lettre par lettre, puis appelle typewriter pour écrire le mot suivant
 * @param {string[]} textes Listes des textes à écrire
 * @param {number} num Indice du mot actuel
 * @param {number} index Indice de la lettre actuel
 */
function deleteWord(textes, num, index) {
    const title = document.querySelector("h1");
    if (index > 0) {
        setTimeout(() => {
            title.removeChild(title.lastChild);
            deleteWord(textes, num, index - 1);
        }, 100);
    } else {
        setTimeout(() => {
            // Si on est au dernier élément du tableau, on recommence avec le premier élément
            if (num + 1 == textes.length) {
                typewriter(textes, 0, index);
            } else {
                typewriter(textes, num + 1, index);
            }
        }, 500);
    }
}
