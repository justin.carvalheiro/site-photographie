/**
 * @property {HTMLElement} element
 * @property {string[]} images Chemins des images de la galerie
 * @property {string[]} descriptions Descriptions des images
 * @property {string} url Image actuel
 */
class Lightbox {
  static init() {
    // Récupère tous les liens des images
    const links = Array.from(
      document.querySelectorAll(
        'a[href$=".png"],a[href$=".jpg"],a[href$=".jpeg"]'
      )
    );
    const gallery = links.map((link) => link.getAttribute("href"));

    // Récupère toutes les descriptions des images
    const textes = Array.from(document.querySelectorAll("img[alt]"));
    const descriptions = textes.map((img) => img.getAttribute("alt"));

    links.forEach((link) =>
      link.addEventListener("click", (e) => {
        e.preventDefault();
        // Récupère la position de l'images pour récupérer la description
        let pos = gallery.findIndex(
          (image) => image == e.currentTarget.getAttribute("href")
        );
        if (pos == gallery.length - 1) {
          pos = -1;
        }

        // Créer la lightbox
        new Lightbox(
          e.currentTarget.getAttribute("href"),
          descriptions[pos],
          gallery,
          descriptions
        );
      })
    );
  }

  /**
   *
   * @param {string} url URL de l'image
   * @param {string} desc Description de l'image
   * @param {string[]} images Chemins des images de la galerie
   * @param {string[]} descriptions Descriptions des images
   */
  constructor(url, desc, images, descriptions) {
    this.element = this.buildDOM(url);
    this.images = images;
    this.descriptions = descriptions;
    this.loadImage(url, desc);
    this.onKeyUp = this.onKeyUp.bind(this);
    document.body.appendChild(this.element);
    document.addEventListener("keyup", this.onKeyUp);
    document.documentElement.style.overflow = "hidden";
  }

  /**
   * Charge l'image
   * @param {string} url Image à charger
   * @param {string} desc Description à charger
   */
  loadImage(url, desc) {
    this.url = null;
    const img = new Image();
    const container = this.element.querySelector(".lightbox_container");
    const loader = document.createElement("div");
    loader.classList.add("lightbox_loader");

    container.innerHTML = "";
    container.appendChild(loader);
    let texte1 = document.createElement("p");
    let texte2 = document.createElement("p");
    texte1.textContent = desc.split(";")[0];
    texte2.textContent = desc.split(";")[1];
    img.onload = () => {
      container.removeChild(loader);
      container.appendChild(img);
      container.appendChild(texte1);
      container.appendChild(texte2);
      this.url = url;
    };
    img.src = url;
  }

  /**
   *
   * @param {KeyboardEvent} e
   */
  onKeyUp(e) {
    if (e.key == "Escape") {
      this.close(e);
    } else if (e.key == "ArrowRight") {
      this.next(e);
    } else if (e.key == "ArrowLeft") {
      this.prev(e);
    }
  }

  /**
   * Ferme la lightbox
   * @param {MouseEvent/KeyboardEvent} e
   */
  close(e) {
    e.preventDefault();
    this.element.classList.add("fadeOut");
    window.setTimeout(() => {
      this.element.parentElement.removeChild(this.element);
    }, 500);
    document.removeEventListener("keyup", this.onKeyUp);
    document.documentElement.style.overflow = "visible";
  }

  /**
   * Passe à l'image suivante
   * @param {MouseEvent/KeyboardEvent} e
   */
  next(e) {
    e.preventDefault();
    let pos = this.images.findIndex((image) => image == this.url);
    if (pos == this.images.length - 1) {
      pos = -1;
    }
    this.loadImage(this.images[pos + 1], this.descriptions[pos + 1]);
  }

  /**
   * Passe à l'image suivante
   * @param {MouseEvent/KeyboardEvent} e
   */
  prev(e) {
    e.preventDefault();
    let pos = this.images.findIndex((image) => image == this.url);
    if (pos == 0) {
      pos = this.images.length;
    }
    this.loadImage(this.images[pos - 1], this.descriptions[pos - 1]);
  }

  /**
   * Construit la partie HTML
   * @param {string} url URL de l'image
   * @returns {HTMLElement}
   */
  buildDOM(url) {
    const dom = document.createElement("div");
    dom.classList.add("lightbox");
    dom.innerHTML = `
        <div class="lightbox">
            <button class="lightbox_close"></button>
            <button class="lightbox_next"></button>
            <button class="lightbox_prev"></button>
            <div class="lightbox_container">
            </div>
        </div>`;
    dom
      .querySelector(".lightbox_close")
      .addEventListener("click", this.close.bind(this));
    dom
      .querySelector(".lightbox_next")
      .addEventListener("click", this.next.bind(this));
    dom
      .querySelector(".lightbox_prev")
      .addEventListener("click", this.prev.bind(this));
    return dom;
  }
}

Lightbox.init();
