// A l'ouverture de la page
window.onload = function () {
  // Si le dark mode est activé on appel DarkMode()
  if (
    getCookie("dark_mode") == "true" ||
    window.matchMedia("prefers-color-scheme: dark)").matches
  ) {
    // On change la valeur de dark_mode
    document.cookie = "dark_mode=false ; path=/";
    DarkMode();
  }
};

/**
 * Animation du bouton dark mode
 * @param {number} nb int pour la condition de fin
 * @param {boolean} bool Connaître le type d'animation (true=animation debut / false=animation fin)
 */
function Button_DarkMode(nb, bool) {
  if (nb == 9) {
    if (bool) return DarkMode();
    return;
  }
  var logo = document.getElementById("dark_mode");
  var t = logo.style.transform.match(/[0-9]+/g) || [0];
  var val = (t[0] + 40) % 360;
  logo.style.transform = "rotate(" + val + "deg)";
  setTimeout(Button_DarkMode, "5", nb + 1, bool);
  logo.blur(); // Enlève le focus de l'élément
}

/**
 * Passe du mode sombre au mode jour et inversement
 */
function DarkMode() {
  // On récupère la valeur du cookie
  var dark_mode = getCookie("dark_mode");

  // Si il n'existe pas, on le créer
  if (dark_mode == null) {
    document.cookie = "dark_mode=false ; path=/";
  }
  var root = document.documentElement;
  if (dark_mode == "true") {
    // Light mode
    document.documentElement.setAttribute("data-theme", "light");
    if (location.href.split("/").pop() == "contact.html") {
      document.getElementById("logo_mail").src =
        "images/ressources/mail_black.svg";
      document.getElementById("logo_insta").src =
        "images/ressources/instagram_black.svg";
      document.getElementById("logo_500px").src =
        "images/ressources/500px_black.svg";
    } else {
      if (location.href.split("/").pop() == "home.html") {
        for (
          i = 0;
          i < document.getElementsByClassName("logo_insta_mini_galerie").length;
          i++
        ) {
          document.getElementsByClassName("logo_insta_mini_galerie")[i].src =
            "images/ressources/instagram_black.svg";
        }
      }
    }

    document.getElementById("dark_mode").style.backgroundImage =
      "url(images/ressources/dark_mode.svg)";
    Button_DarkMode(0, false);
    document.cookie = "dark_mode=false ; path=/";
  } else {
    // Dark mode
    document.documentElement.setAttribute("data-theme", "dark");
    if (location.href.split("/").pop() == "contact.html") {
      document.getElementById("logo_mail").src =
        "images/ressources/mail_white.svg";
      document.getElementById("logo_insta").src =
        "images/ressources/instagram_white.svg";
      document.getElementById("logo_500px").src =
        "images/ressources/500px_white.svg";
    } else {
      if (location.href.split("/").pop() == "home.html") {
        for (
          i = 0;
          i < document.getElementsByClassName("logo_insta_mini_galerie").length;
          i++
        ) {
          document.getElementsByClassName("logo_insta_mini_galerie")[i].src =
            "images/ressources/instagram_white.svg";
        }
      }
    }

    document.getElementById("dark_mode").style.backgroundImage =
      "url(images/ressources/light_mode.svg)";
    Button_DarkMode(0, false);
    document.cookie = "dark_mode=true ; path=/";
  }
}

/**
 * Retourne la valeur du cookie recherché
 * @param {string} name Nome du cookie
 * @returns Valeur du cookie ou null si il n'existe pas
 */
function getCookie(name) {
  var cookies = document.cookie.split("; ");
  for (var i in cookies) {
    if (cookies[i].split("=")[0] === name) {
      return cookies[i].split("=")[1];
    }
  }
  return null;
}
