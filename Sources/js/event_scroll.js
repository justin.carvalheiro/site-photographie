showShadow();
window.onscroll = function () {
  showShadow();
};

var hiddenLimit = 300;
if (screen.width < 750) {
  hiddenLimit = 90;
}
var lastX = window.scrollY;

/**
 * Ajoute une box-shadow à la barre de navigation lorsque l'utilisateur à scrollé plus de 25px
 * Cache / Montre la barre de navigation en fonction du scroll de l'utilisateur
 */
function showShadow() {
  var x = window.scrollY;
  var nav = document.getElementById("nav");
  // Box-Shadow
  if (x >= 25) {
    nav.style.boxShadow = "0 6px 14px rgb(0 0 0 / 25%)";
  } else {
    nav.style.boxShadow = "none";
  }
  // Position
  if (x > hiddenLimit) {
    nav.style.top = "-100px";
  } else {
    nav.style.top = "10px";
  }
  if (x < lastX) {
    nav.style.top = "10px";
  }
  lastX = x;
}
